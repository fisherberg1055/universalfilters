To install project add next line to devDependencies:

<i>`universalFilters`</i>

or run in terminal 

<i>`npm install universalFilters` </i>
<hr>
<h3><u>FilterSelect</u></h3>

<h4>How to use</h4>

<p>Import component:</p>

`import {UniversalFilter}  from 'vue_reusable_components'` 
<p>includes to components list:</p>

`components: { UniversalFilter }` 

<p>Using in the templates:</p>

`<UniversalFilter></UniversalFilter>` 

<h4>The types of the inputs & selects</h4>

<ol>
    <li>DatePicker</li>
    <li>Select</li>
    <li>Input</li>
    <li>Checkbox</li>
</ol>

<h4>The input parameters</h5>
<table>
    <tr>
        <th>Property</th>
        <th>Type</th>
        <th>Default</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>id</td>
        <td>string</td>
        <td> - </td>
        <td>Id параметра, будет использовано для передачи на бек в фильтрах</td>
    </tr>
    <tr>
        <td>name</td>
        <td>string</td>
        <td>-</td>
        <td>Имя компонента, будет использовано в теге Lable.</td>
    </tr>
    <tr>
        <td>type</td>
        <td>string</td>
        <td>'Select'</td>
        <td>Тип компонента (date, input, select, radio )</td>
    </tr>
    <tr>
        <td>url</td>
        <td>string</td>
        <td>-</td>
        <td>Url для получения опций c бека для наполнения компонента.</td>
    </tr>
    <tr>
        <td>options</td>
        <td>array</td>
        <td>-</td>
        <td>Готовые опции для наполнения компонента.</td>
    </tr>
    <tr>
        <td>class</td>
        <td>string</td>
        <td>-</td>
        <td>Оберточный класс для компонента</td>
    </tr>
    <tr>
        <td>multiple</td>
        <td>boolean</td>
        <td>-</td>
        <td>Возможность множественного выбора</td>
    </tr>
    <tr>
        <td>need_all</td>
        <td>boolean</td>
        <td>-</td>
        <td>Добавление опции “Все”</td>
    </tr>
    <tr>
        <td>depends_from</td>
        <td>string</td>
        <td>-</td>
        <td>Добавление зависимости от id другого компонента</td>
    </tr>
    <tr>
        <td>minimum_view</td>
        <td>string</td>
        <td>id</td>
        <td>Минимальный вид (day, month,year) для календаря</td>
    </tr>
    <tr>
        <td>is_group</td>
        <td>boolean</td>
        <td>-</td>
        <td>Признак добавления группировки</td>
    </tr>
</table>

</hr>  