
export interface FilterCustomOptions  {
    attr_value: string | number | null;
    attr_text: string;
}

export interface FilterSelectObj {
    id: string;
    name: string;
    type?: string;
    url?: string;
    options?: Array<FilterCustomOptions>;
    class?: string;
    multiple?: boolean;
    need_all?: boolean;
    depends_from?: string;
    minimum_view?: string;
    is_group?: boolean;
}