import Vue, { PluginFunction, VueConstructor } from 'vue';


interface InstallFunction extends PluginFunction<any> {
  installed?: boolean;
}

declare const Vue_reusable_components: { install: InstallFunction };
export default Vue_reusable_components;

export const Vue_reusable_componentsSample: VueConstructor<Vue>;

